<?php

namespace Drupal\eca_content_access\Plugin\Action;

/**
 * Revoke content access from a node entity for a role.
 *
 * @Action(
 *   id = "eca_content_access_revoke_access",
 *   label = @Translation("Content access: revoke access"),
 *   description = @Translation("Revoke access from an individual node for a role to overwrite settings of the content type."),
 *   eca_version_introduced = "1.0.0",
 *   type = "node"
 * )
 */
class RevokeAccess extends GrantAccess {

  /**
   * {@inheritdoc}
   */
  protected function updateSettings(array &$bundleSettings, string $operation, string $role): bool {
    if (!isset($bundleSettings[$operation]) || !in_array($role, $bundleSettings[$operation], TRUE)) {
      // The role already has no access to that operation, nothing to change.
      return FALSE;
    }
    $bundleSettings[$operation] = array_diff($bundleSettings[$operation], [$role]);
    return TRUE;
  }

}
