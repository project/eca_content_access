<?php

namespace Drupal\eca_content_access\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\node\NodeGrantDatabaseStorageInterface;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Grants content access to a node entity for a role.
 *
 * @Action(
 *   id = "eca_content_access_grant_access",
 *   label = @Translation("Content access: grant access"),
 *   description = @Translation("Grants access to an individual node for a role to overwrite settings of the content type."),
 *   eca_version_introduced = "1.0.0",
 *   type = "node"
 * )
 */
class GrantAccess extends ConfigurableActionBase {

  /**
   * The content_access settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $contentAccessSettings;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The node grant storage.
   *
   * @var \Drupal\node\NodeGrantDatabaseStorageInterface
   */
  protected NodeGrantDatabaseStorageInterface $grantStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->contentAccessSettings = $container->get('config.factory')->get('content_access.settings');
    $instance->database = $container->get('database');
    $instance->grantStorage = $container->get('node.grant_storage');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'operation' => '',
      'role' => '',
      'follow_up' => 'none',
      'clear_cache' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#default_value' => $this->configuration['operation'],
      '#options' => [
        'view' => $this->t('View any content'),
        'view_own' => $this->t('View own content'),
        'update' => $this->t('Edit any content'),
        'update_own' => $this->t('Edit own content'),
        'delete' => $this->t('Delete any content'),
        'delete_own' => $this->t('Delete own content'),
      ],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    $roles = [];
    /** @var \Drupal\user\RoleInterface $role */
    foreach (Role::loadMultiple() as $role) {
      $roles[$role->id()] = $role->label();
    }
    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('User role'),
      '#default_value' => $this->configuration['role'],
      '#options' => $roles,
      '#required' => TRUE,
      '#weight' => -9,
    ];
    $form['follow_up'] = [
      '#type' => 'select',
      '#title' => $this->t('Follow up'),
      '#default_value' => $this->configuration['follow_up'],
      '#options' => [
        'none' => $this->t('Do nothing'),
        'display_message' => $this->t('Display message with link to rebuild'),
        'rebuild' => $this->t('Rebuild access directly'),
      ],
      '#required' => TRUE,
      '#weight' => -8,
    ];
    $form['clear_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clear cache'),
      '#default_value' => $this->configuration['clear_cache'],
      '#weight' => -7,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['operation'] = $form_state->getValue('operation');
    $this->configuration['role'] = $form_state->getValue('role');
    $this->configuration['follow_up'] = $form_state->getValue('follow_up');
    $this->configuration['clear_cache'] = $form_state->getValue('clear_cache');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (!($object instanceof NodeInterface) || empty($object->id())) {
      $access_result = AccessResult::forbidden('Entity is not a node or has no ID yet.');
    }
    else {
      $settings = $this->contentAccessSettings->get('content_access_node_type');
      if (empty($settings) || empty($settings[$object->bundle()])) {
        $access_result = AccessResult::forbidden('No content access settings available for this content type.');
      }
      else {
        $bundleSettings = unserialize($settings[$object->bundle()], ['']);
        if (empty($bundleSettings['per_node'])) {
          $access_result = AccessResult::forbidden('Per node settings not allowed for this content type.');
        }
        else {
          $access_result = AccessResult::allowed();
        }
      }
    }
    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?NodeInterface $entity = NULL): void {
    if ($entity === NULL) {
      return;
    }
    $record = $this->database->select('content_access', 'ca')
      ->fields('ca', ['settings'])
      ->condition('ca.nid', $entity->id())
      ->execute()
      ->fetchField();
    if (empty($record)) {
      $settings = $this->contentAccessSettings->get('content_access_node_type');
      $bundleSettings = unserialize($settings[$entity->bundle()], ['']);
      unset($bundleSettings['per_node']);
    }
    else {
      $bundleSettings = json_decode($record, TRUE);
    }
    if ($this->updateSettings($bundleSettings, $this->configuration['operation'], $this->configuration['role'])) {
      $this->database->merge('content_access')
        ->keys(['nid'], [$entity->id()])
        ->fields(['settings' => json_encode($bundleSettings)])
        ->execute();
      /**
       * @var \Drupal\node\NodeAccessControlHandler $controlHandler
       */
      $controlHandler = $this->entityTypeManager->getAccessControlHandler('node');
      // Apply new settings.
      $grants = $controlHandler->acquireGrants($entity);
      $this->grantStorage->write($entity, $grants);
    }

    if ($this->configuration['clear_cache']) {
      foreach (Cache::getBins() as $cache_backend) {
        $cache_backend->deleteAll();
      }
    }
    switch ($this->configuration['follow_up']) {
      case 'none':
        // We have nothing to do.
        break;

      case 'display_message':
        $this->messenger()->addMessage($this->t('Your changes have been saved. You may have to <a href=":rebuild">rebuild permissions</a> for your changes to take effect.',
          [':rebuild' => Url::fromRoute('node.configure_rebuild_confirm')->toString()]));
        break;

      case 'rebuild':
        $this->messenger()->addMessage($this->t('Direct rebuild not possible at this point. You may have to <a href=":rebuild">rebuild permissions</a> for your changes to take effect.',
          [':rebuild' => Url::fromRoute('node.configure_rebuild_confirm')->toString()]));
        break;

    }
  }

  /**
   * Performs the setting changes.
   *
   * @param array $bundleSettings
   *   The current settings for that node.
   * @param string $operation
   *   The operation for which the settings should be changed.
   * @param string $role
   *   The role for which the settings should be changed.
   */
  protected function updateSettings(array &$bundleSettings, string $operation, string $role): bool {
    if (isset($bundleSettings[$operation]) && in_array($role, $bundleSettings[$operation], TRUE)) {
      // The role already has access to that operation, nothing to change.
      return FALSE;
    }
    $bundleSettings[$operation][] = $role;
    return TRUE;
  }

}
